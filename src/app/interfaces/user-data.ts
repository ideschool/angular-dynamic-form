import {Address} from './address';

export interface UserData {
  firstName: string;
  lastName: string;
  email: string;
  addresses: Address[];
}
