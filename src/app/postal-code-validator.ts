import {AbstractControl} from '@angular/forms';

export function postalCodeValidator (control: AbstractControl): {[key: string]: any} {
  const value: string = control.value;
  const patt: RegExp = /\d{2}-\d{3}/;
  return patt.test(value) ? {'postal-code': false} : null;
}
