import {Component} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, Validators} from '@angular/forms';
import {postalCodeValidator} from './postal-code-validator';
import {Address} from './interfaces/address';
import {UserData} from './interfaces/user-data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: []
})
export class AppComponent {
  contactForm: FormGroup;
  addressTypes: string[] = ['pocztowy', 'domowy', 'praca', 'inny'];

  constructor(private fb: FormBuilder) {
    this.buildForm();
  }

  get addresses(): FormArray {
    return this.contactForm.get('address') as FormArray;
  }

  buildForm(): void {
    this.contactForm = this.fb.group({
      'first-name': new FormControl('', Validators.required),
      'last-name': new FormControl('', Validators.required),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'address': this.fb.array([]),
    });
    this.addAddress();
  }

  getAddressGroup(): FormGroup {
    return new FormGroup({
      'type': new FormControl(this.addressTypes[0], Validators.required),
      'street': new FormControl('', Validators.required),
      'city': new FormControl('', Validators.required),
      'postal-code': new FormControl('', [Validators.required, postalCodeValidator]),
    });
  }

  addAddress(): void {
    this.addresses.push(this.getAddressGroup());
  }

  removeAddress(idx: number) {
    this.addresses.removeAt(idx);
  }

  getUser(): UserData {
    const data: UserData = {
      firstName: this.contactForm.get('first-name').value,
      lastName: this.contactForm.get('last-name').value,
      email: this.contactForm.get('email').value,
      addresses: [] as Address[],
    };
    for (let i = 0; i < this.addresses.length; i++) {
      data.addresses.push(this.getAddress(i));
    }
    return data;
  }

  getAddress(idx: number): Address {
    const group: FormGroup = this.addresses.controls[idx] as FormGroup;
    return {
      street: group.get('street').value,
      city: group.get('city').value,
      postalCode: group.get('postal-code').value,
      type: group.get('type').value,
    };
  }
  save() {
    const userData = this.getUser();
    console.log(userData);
  }
}
